#!/usr/bin/env bash

echo ">>> Starting Install Script"

# Update
sudo apt-get update

# Install MySQL without prompt
sudo debconf-set-selections <<< 'mariadb-server mysql-server/root_password password vagrant'
sudo debconf-set-selections <<< 'mariadb-server mysql-server/root_password_again password vagrant'


echo ">>> Installing Base Items"

# Install base items
sudo apt-get install -y vim tmux curl wget build-essential python-software-properties

echo ">>> Adding PPA's and Installing Server Items"

# Add repo for latest PHP
sudo add-apt-repository ppa:ondrej/php

# Update Again
sudo apt-get update

# Install the Rest
sudo apt-get install -y git-core php7.1 php7.1-common apache2 libapache2-mod-php7.1 php7.1-curl php7.1-xml php7.1-zip php7.1-gd php7.1-mysql php7.1-mbstring libphp-phpmailer mariadb-server mariadb-client

echo ">>> Configuring Server"

# Apache Config
sudo a2enmod rewrite
#curl https://gist.github.com/fideloper/2710970/raw/vhost.sh > vhost
#sudo chmod guo+x vhost
#sudo mv vhost /usr/local/bin

# PHP Config
sed -i "s/error_reporting = .*/error_reporting = E_ALL/" /etc/php/7.1/apache2/php.ini
sed -i "s/display_errors = .*/display_errors = On/" /etc/php/7.1/apache2/php.ini

sudo service apache2 restart

# Git Config and set Owner
curl https://gist.github.com/fideloper/3751524/raw/.gitconfig > /home/ubuntu/.gitconfig
sudo chown ubuntu:ubuntu /home/ubuntuubuntu/.gitconfig

echo ">>> Installing Composer"

# Composer
curl -sS https://getcomposer.org/installer | php
sudo mv composer.phar /usr/local/bin/composer

echo ">>> Setting up Vim"

DIRECTORY="/home/ubuntu/.vim"

if [ -d "$DIRECTORY" ]; then
  echo ">>> Removing '.vi' Directory"
  sudo chown -R ubuntu:ubuntu /home/ubuntu/.vim
  sudo rm -rf $DIRECTORY
fi

# Create directories needed for some .vimrc settings
mkdir -p /home/ubuntu/.vim/backup
mkdir -p /home/ubuntu/.vim/swap

# Install Vundle and set finally owner of .vim files
git clone https://github.com/gmarik/vundle.git /home/ubuntu/.vim/bundle/vundle
sudo chown -R ubuntu:ubuntu /home/ubuntu/.vim

# Grab my .vimrc and set owner
curl https://gist.github.com/fideloper/a335872f476635b582ee/raw/.vimrc > /home/ubuntu/.vimrc
sudo chown ubuntu:ubuntu /home/ubuntu/.vimrc

echo ">>> Ready!"
